

/**
 * Created by willi on 04/02/2016.
 */
public class Case {
    private int posX1,posX2,posY1,posY2;
    private int type;
    /*
        type :
            0 = Libre
            1 = Cases mur
            2 = Teleporteur
            3 =
            4 =
            5 =BouleInvincible

     */
    private boolean libre;

    public Case(int a,int b, int c, int d,int type){
        this.posX1=a;
        this.posX2=b;
        this.posY1=c;
        this.posY2=d;
        this.type=type;
        if(this.type==0 || this.type==5){
            this.libre=true;
        }
        else{
            this.libre=false;
        }
    }
    public int getType(){
        return this.type;
    }
    // bizarre marche pour droite et bas //
    public boolean DeplacementPossible(){

        if(this.type==0 || this.type==2 ||this.type==3 || this.type==5){

            return true;
        }
        else{
            return false;
        }
    }


    public void setType(int num){
        this.type=num;
    }

    public Pacman getPacman(){
        return ControleurDebut.f.getControl().getPacman();
    }


    public String toString(){
          return "Case X : "+posX1+","+posX2+" Case Y : "+posY1+","+posY2+".";
      }
    public int getPosX(){
        return this.posX1;
    }
    public int getPosX2(){
        return this.posX2;
    }
    public int getPosY(){
        return this.posY1;
    }

    public int getPosY2(){return this.posY2 ;}

}
